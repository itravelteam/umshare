


import React, { Component, PropTypes } from 'react';
import { 
    requireNativeComponent,
} from 'react-native';

var SB = requireNativeComponent('MyShareBt', MyShareBt);

export default class MyShareBt extends Component {

    static propTypes = {
    /**
    *
    * 定义组件需要传到原生端的属性
    * 使用React.PropTypes来进行校验
    */
    
    //分享的网页地址
    webpageUrl:PropTypes.string,

    //要分享的内容
    shareText:PropTypes.string,

    //分享按钮标题
    myTitle:PropTypes.string,

    //分享按钮标题颜色
    color:PropTypes.string,

    //分享按钮图片
    btImageName:PropTypes.string,

    //按钮点击事件
    onButtonClicked:PropTypes.func,

    //按钮背景色
    backgroundColor:PropTypes.string, // 这个貌似不需要在 Xcode 中写也可以直接在JS中设置
  };

  render() {
    return (
      <SB {...this.props} />
    );
  }
}
