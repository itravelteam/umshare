/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import MyShareBt from './MyShareBt.js';
import AuthLoginBt from './AuthLoginBt.js';

        // <Text style={styles.welcome}>
        //   Welcome to React Native!
        // </Text>
        // <Text style={styles.instructions}>
        //   To get started, edit index.ios.js
        // </Text>
        // <Text style={styles.instructions}>
        //   Press Cmd+R to reload,{'\n'}
        //   Cmd+D or shake for dev menu
        // </Text>

export default class ShareSDKDemo extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loginBtTitle: '三方登陆'
    };
  }
  


  render() {
    var that = this;

    return (
      <View style={styles.container}>

        <MyShareBt
            style = {styles.map}

            // 分享内容属性
            shareText = {'JS自定义的分享文字！ http://bbs.reactnative.cn/topic/2452/react-native%E5%8E%9F%E7%94%9Fui%E5%B0%81%E8%A3%85%E4%BB%A5%E5%8F%8A%E4%BA%8B%E4%BB%B6%E5%A4%84%E7%90%86'}
            webpageUrl = {'http://mobile.umeng.com/social'}

            // 分享按钮属性
            myTitle = {'JS自定义的bt文字'}//设置分享按钮标题
            color={'red'}//设置分享按钮标题字体颜色
            btImageName = {'share_icon'} //设置分享按钮图片
            onButtonClicked = {(event) => {
              console.log('React 分享按钮点击' + event.nativeEvent.randomValue);
            }}

            // 这个貌似不用在 Xcode 中写便可以指定按钮背景颜色
            backgroundColor = {'blue'}
          ></MyShareBt>

          <AuthLoginBt 

            style = {styles.authBt}
            myTitle = {that.state.loginBtTitle}
            color = {'green'}
            // btImageName = {'logo'}
            backgroundColor = {'orange'}

            onLoginSuccess = {(event) => {
              console.log('React 三方登陆成功后的用户名' + event.nativeEvent.authName)
              
              that.setState({
                loginBtTitle : event.nativeEvent.authName
              })

            }}

            onLoginFail = {(event) => {
              console.log('React 三方登陆失败：' + event.nativeEvent.errorInfo)
              
              that.setState({
                loginBtTitle : event.nativeEvent.errorInfo
              })

            }}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  map: {
    width:100,
    height:60,
    // backgroundColor:'#F8F8F8',
    alignItems:'center',
    justifyContent:'center',
    // marginTop: 40,
    // textAlign: 'center',
  },
  authBt: {
    width:100,
    height:60,
    // backgroundColor:'#F8F8F8',
    alignItems:'center',
    justifyContent:'center',
    marginTop: 100,
    // textAlign: 'center',
  }
});

AppRegistry.registerComponent('ShareSDKDemo', () => ShareSDKDemo);
