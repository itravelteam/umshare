

import React, { Component, PropTypes } from 'react';
import {
	requireNativeComponent,
} from 'react-native';

var AuthBt = requireNativeComponent('AuthLoginBt', AuthLoginBt);

export default class AuthLoginBt extends Component {
	static propTypes = {

    authName:PropTypes.string,

		myTitle:PropTypes.string,

		color:PropTypes.string,

		btImageName:PropTypes.string,

		onButtonClicked:PropTypes.func,

    onLoginSuccess:PropTypes.func,



		backgroundColor:PropTypes.string,
	};

	render() {
		return (
			<AuthBt {...this.props} />
			);
	}
}
