//
//  MyShareBtManager.m
//  ShareSDKDemo
//
//  Created by Jianing Zheng on 1/30/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "MyShareBtManager.h"
#import <UMSocialCore/UMSocialCore.h>
#import "MyShareBt.h"

@implementation MyShareBtManager

RCT_EXPORT_MODULE()

//将所需参数导出给JS
RCT_EXPORT_VIEW_PROPERTY(webpageUrl, NSString)
RCT_EXPORT_VIEW_PROPERTY(shareText, NSString)

RCT_EXPORT_VIEW_PROPERTY(myTitle, NSString)
RCT_EXPORT_VIEW_PROPERTY(color, UIColor)
RCT_EXPORT_VIEW_PROPERTY(btImageName, NSString)

// 事件的导出，onClickBanner对应view中扩展的属性
RCT_EXPORT_VIEW_PROPERTY(onButtonClicked, RCTBubblingEventBlock)

- (UIView *)view
{
  MyShareBt *bt = [[MyShareBt alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
  return bt;
}

@end
