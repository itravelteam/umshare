//
//  MyShareBt.h
//  ShareSDKDemo
//
//  Created by Jianing Zheng on 1/30/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>

@interface MyShareBt: UIButton

// 分享内容的属性
@property (nonatomic, copy) NSString * shareText; // 分享的文本
@property (nonatomic, copy) NSString * webpageUrl; // 分享的网页地址

// 分享按钮的属性
@property (nonatomic, copy) NSString * myTitle; // 分享按钮标题
@property (nonatomic) UIColor * color; // 按钮字体颜色
@property (nonatomic, copy) NSString * btImageName; // 按钮背景图片

/** button点击事件*/
@property (nonatomic, copy) RCTBubblingEventBlock onButtonClicked;

@end
