//
//  AuthLoginBt.m
//  ShareSDKDemo
//
//  Created by Jianing Zheng on 2/3/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "AuthLoginBt.h"
#import <UMSocialCore/UMSocialCore.h>

@implementation AuthLoginBt

- (instancetype)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  }
  return self;
}

// override
- (void)setMyTitle:(NSString *)myTitle {
  [self setTitle:myTitle forState:UIControlStateNormal];
}
- (void)setColor:(UIColor *)color {
  [self setTitleColor:color forState:UIControlStateNormal];
}
- (void)setBtImageName:(NSString *)btImageName{
  [self setBackgroundImage:[UIImage imageNamed:btImageName] forState:UIControlStateNormal];
}

// 点击事件
- (void)loginButtonClicked {
  
//  // 第三方登陆 - 新浪微博
//  [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_Sina currentViewController:nil completion:^(id result, NSError *error) {
//    if (error) {
//      UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"新浪微博授权登陆出错！" message:[error localizedDescription] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//      [alertView show];
//    } else {
//      UMSocialUserInfoResponse *resp = result;
//      
//      // 授权信息
//      NSLog(@"Sina uid: %@", resp.uid);
//      NSLog(@"Sina accessToken: %@", resp.accessToken);
//      NSLog(@"Sina refreshToken: %@", resp.refreshToken);
//      NSLog(@"Sina expiration: %@", resp.expiration);
//      
//      // 用户信息
//      NSLog(@"Sina name: %@", resp.name);
//      NSLog(@"Sina iconurl: %@", resp.iconurl);
//      NSLog(@"Sina gender: %@", resp.gender);
//      
//      // 第三方平台SDK源数据
//      NSLog(@"Sina originalResponse: %@", resp.originalResponse);
//      
//  }];
  
  // 第三方登陆 - 微信
  [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
    if (error) {
      
      UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"微信授权登陆出错！" message:[error localizedDescription] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
      [alertView show];
      
      self.onLoginFail(@{@"errorInfo": error.localizedDescription});

    } else {
      UMSocialUserInfoResponse *resp = result;
      
      // 授权信息
      NSLog(@"Wechat uid: %@", resp.uid);
      NSLog(@"Wechat openid: %@", resp.openid);
      NSLog(@"Wechat accessToken: %@", resp.accessToken);
      NSLog(@"Wechat refreshToken: %@", resp.refreshToken);
      NSLog(@"Wechat expiration: %@", resp.expiration);
      
      // 用户信息
      NSLog(@"Wechat name: %@", resp.name);
      NSLog(@"Wechat iconurl: %@", resp.iconurl);
      NSLog(@"Wechat gender: %@", resp.gender);
      
      // 第三方平台SDK源数据
      NSLog(@"Wechat originalResponse: %@", resp.originalResponse);
      
      self.onLoginSuccess(@{@"authName": resp.name});
    }
  }];
}


@end
