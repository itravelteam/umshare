//
//  AuthLoginBt.h
//  ShareSDKDemo
//
//  Created by Jianing Zheng on 2/3/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>

@interface AuthLoginBt : UIButton

// 按钮的属性
@property (nonatomic, copy) NSString * myTitle; // 标题
@property (nonatomic) UIColor * color; // 字体颜色
@property (nonatomic, copy) NSString * btImageName; // 背景图片

/** 登陆失败回调*/
@property (nonatomic, copy) RCTBubblingEventBlock onLoginFail;

/** 登陆成功回调 **/
@property (nonatomic, copy) RCTBubblingEventBlock onLoginSuccess;


@end
