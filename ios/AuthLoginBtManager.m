//
//  AuthLoginBtManager.m
//  ShareSDKDemo
//
//  Created by Jianing Zheng on 2/3/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "AuthLoginBtManager.h"
#import "AuthLoginBt.h"

@implementation AuthLoginBtManager

RCT_EXPORT_MODULE()

// 参数按钮导出到JS
RCT_EXPORT_VIEW_PROPERTY(myTitle, NSString)
RCT_EXPORT_VIEW_PROPERTY(color, UIColor)
RCT_EXPORT_VIEW_PROPERTY(btImageName, NSString)

// 回调参数导出到JS
RCT_EXPORT_VIEW_PROPERTY(authName, NSString)

// 点击事件导出JS
RCT_EXPORT_VIEW_PROPERTY(onButtonClicked, RCTBubblingEventBlock)

// 回传登陆callback到JS
RCT_EXPORT_VIEW_PROPERTY(onLoginSuccess, RCTBubblingEventBlock)

- (UIView *)view {
  AuthLoginBt *bt = [[AuthLoginBt alloc] initWithFrame:CGRectZero];
  return bt;
}

@end
