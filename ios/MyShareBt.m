//
//  MyShareBt.m
//  ShareSDKDemo
//
//  Created by Jianing Zheng on 1/30/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyShareBt.h"
#import <UShareUI/UShareUI.h>

@implementation MyShareBt

//分享按钮初始化
- (instancetype) initWithFrame:(CGRect)frame{
  if ((self = [super initWithFrame:frame])) {
    [self addTarget:self action:@selector(shareToPf)
   forControlEvents:UIControlEventTouchUpInside];
  }
  return self;
}
//重写所传递参数的set方法，并将传递过来的参数用于设置UI
- (void)setMyTitle:(NSString *)myTitle{
  [self setTitle:myTitle forState:UIControlStateNormal];
}

- (void)setColor:(UIColor *)color{
  [self setTitleColor:color forState:UIControlStateNormal];
}

- (void)setBtImageName:(NSString *)btImageName{
  [self setBackgroundImage:[UIImage imageNamed:btImageName] forState:UIControlStateNormal];
}

// 按钮分享事件
- (void)shareToPf {
  
  [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
    switch (platformType) {
      case UMSocialPlatformType_WechatSession:
      {
        //创建分享消息对象
        UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
        
        //创建网页内容对象
        NSString* thumbURL =  @"https://mobile.umeng.com/images/pic/home/social/img-1.png"; // （友盟官方文档：缩略图（UIImage或者NSData类型，或者image_url）） 自己项目里可用项目的appIcon
        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:@"写死的title" descr:_shareText thumImage:thumbURL];
        //设置网页地址
        shareObject.webpageUrl = _webpageUrl; // 分享的网页地址，JS里定制
        
        //分享消息对象设置分享内容对象
        messageObject.shareObject = shareObject;
        
        //调用分享接口
        [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
          if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
            
            UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"分享到微信聊天出错！" message:@"。。。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView show];
          }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
              UMSocialShareResponse *resp = data;
              //分享结果消息
              UMSocialLogInfo(@"response message is %@",resp.message);
              //第三方原始返回的数据
              UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
            }else{
              UMSocialLogInfo(@"response data is %@",data);
            }
            
            UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"分享成功！" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alertView show];
          }
        }];
      }
        break;
        
      default:
        break;
    }
  }];
}

@end

